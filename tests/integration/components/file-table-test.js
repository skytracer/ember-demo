import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { a11yAudit } from 'ember-a11y-testing/test-support';

module('Integration | Component | file-table', function (hooks) {
  setupRenderingTest(hooks);
  const files = [
    {
      name: 'smss.exe',
      device: 'Stark',
      path: '\\Device\\HarddiskVolume2\\Windows\\System32\\smss.exe',
      status: 'scheduled',
    },

    {
      name: 'netsh.exe',
      device: 'Targaryen',
      path: '\\Device\\HarddiskVolume2\\Windows\\System32\\netsh.exe',
      status: 'available',
    },

    {
      name: 'uxtheme.dll',
      device: 'Lanniester',
      path: '\\Device\\HarddiskVolume1\\Windows\\System32\\uxtheme.dll',
      status: 'available',
    },

    {
      name: 'cryptbase.dll',
      device: 'Martell',
      path: '\\Device\\HarddiskVolume1\\Windows\\System32\\cryptbase.dll',
      status: 'scheduled',
    },

    {
      name: '7za.exe',
      device: 'Baratheon',
      path: '\\Device\\HarddiskVolume1\\temp\\7za.exe',
      status: 'scheduled',
    },
  ];
  hooks.beforeEach(async function () {
    this.set('files', files);
    this.set('onDonwload', () => {});
    await render(
      hbs`<FileTable @files={{this.files}} @onDownload={{this.onDonwload}}/>`
    );
  });

  test('test renders', async function (assert) {
    await a11yAudit();
    assert
      .dom(this.element.querySelector('table'))
      .hasAria('label', 'select files to download');
  });

  test('should have the same number of rows as data', async function (assert) {
    const cbs = this.element.querySelectorAll('tr');
    assert.strictEqual(cbs.length, files.length + 1);
  });

  test('should have disabled checkboxes', async function (assert) {
    const cbs = this.element.querySelectorAll('table input');
    files.forEach((f, k) => {
      assert.dom(cbs[k]).hasProperty('disabled', f.status !== 'available');
    });
  });

  test('should reflect select-all checkbox', async function (assert) {
    await click('.select-all_cb input');
    let cbs = this.element.querySelectorAll('tr[aria-selected=true]').length;
    assert.strictEqual(
      cbs,
      files.filter((f) => f.status === 'available').length
    );
    await click('.select-all_cb input');
    cbs = this.element.querySelectorAll('tr[aria-selected=true]').length;
    assert.strictEqual(cbs, 0);
    await click('table input:not(:disabled)');
    assert
      .dom(this.element.querySelector('.select-all_cb input'))
      .hasClass('indeterminate');
    await click('.select-all_cb input');
    cbs = this.element.querySelectorAll('tr[aria-selected=true]').length;
    assert.strictEqual(
      cbs,
      files.filter((f) => f.status === 'available').length
    );
  });

  test('should reflect status when checked', async function (assert) {
    assert
      .dom(this.element.querySelector('[role=status]'))
      .hasText('None Selected');
    await click('.select-all_cb input');
    await a11yAudit();
    assert
      .dom(this.element.querySelector('[role=status]'))
      .hasText('Selected 2');
    await click('table input:not(:disabled)');
    assert
      .dom(this.element.querySelector('[role=status]'))
      .hasText('Selected 1');
  });

  test('should reflect download ability', async function (assert) {
    let isCalled = false;
    let selectedFiles = [];
    this.set('onDonwload', (dldFiles) => {
      isCalled = true;
      selectedFiles = dldFiles;
    });
    assert.dom(this.element.querySelector('button')).isDisabled();
    await click('.select-all_cb input');
    await click('button');
    assert.true(isCalled);
    assert.strictEqual(
      JSON.stringify(selectedFiles),
      JSON.stringify(files.filter((f) => f.status === 'available'))
    );
  });
});
