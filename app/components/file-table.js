import Component from '@glimmer/component';
import { tracked, cached } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class FileTableComponent extends Component {
  @tracked vSelected = new WeakMap(this.args.files.map((f) => [f, false]));

  @action
  selectFile(file, { target: { checked } }) {
    this.vSelected.set(file, checked);
    this.vSelected = this.vSelected;
  }

  @action
  selectAll() {
    const doSelectAll = this.selectable.length !== this.selected.length;
    this.args.files.forEach((file) => {
      this.vSelected.set(file, file.status === 'available' && doSelectAll);
    });
    this.vSelected = this.vSelected;
  }

  @cached
  get selectable() {
    return this.args.files.filter((file) => file.status === 'available');
  }

  get partialSelected() {
    return (
      this.selectable.length > this.selected.length && this.selected.length
    );
  }

  get selected() {
    return this.args.files.filter((file) => this.vSelected.get(file));
  }
}
