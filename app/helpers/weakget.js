import { helper } from '@ember/component/helper';

function weakget([weakmap, prop]) {
  return weakmap.get(prop);
}

export default helper(weakget);
